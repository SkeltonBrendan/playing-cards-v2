//Playing Cards
//Jason Le

// Edited by Brendan Skelton
// on 9/11/19 & 9/12/19

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	two = 2, three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace
};

enum Suit
{
	heart, diamond, club, spade
};

struct Card
{
	Rank rank;
	Suit suit;
};


void PrintCard(Card card) 
{

	switch (card.rank) 
	{
	case two: cout << "The Two of ";
		break;
	case three: cout << "The Three of ";
		break;
	case four: cout << "The Four of ";
		break;
	case five: cout << "The Five of ";
		break;
	case six: cout << "The Six of ";
		break;
	case seven: cout << "The Seven of ";
		break;
	case eight: cout << "The Eight of ";
		break;
	case nine: cout << "The Nine of ";
		break;
	case ten: cout << "The Ten of ";
		break;
	case jack: cout << "The Jack of ";
		break;
	case queen: cout << "The Queen of ";
		break;
	case king: cout << "The King of ";
		break;
	case ace: cout << "The Ace of ";
		break;
	default:
		cout << "Not a card rank";
	}
	switch (card.suit)
	{
	case heart: cout << "Heart.\n";
		break;
	case diamond: cout << "Diamond.\n";
		break;
	case club: cout << "Club.\n";
		break;
	case spade: cout << "Spade.\n";
		break;
	default:
		cout << "Not a card rank";
	}

}

Card HighCard(Card card1, Card card2) 
{
	int rank1;
	int rank2;
	switch (card1.rank)
	{
	case two: rank1 = 2;
		cout << "The first card is a Two of ";
		break;
	case three: rank1 = 3;
		cout << "The first card is a Three of ";
		break;
	case four: rank1 = 4;
		cout << "The first card is a Four of ";
		break;
	case five: rank1 = 5;
		cout << "The first card is a Five of ";
		break;
	case six: rank1 = 6;
		cout << "The first card is a Six of ";
		break;
	case seven: rank1 = 7;
		cout << "The first card is a Seven of ";
		break;
	case eight: rank1 = 8;
		cout << "The first card is a Eight of ";
		break;
	case nine: rank1 = 9;
		cout << "The first card is a Nine of ";
		break;
	case ten: rank1 = 10;
		cout << "The first card is a Ten of ";
		break;
	case jack: rank1 = 11;
		cout << "The first card is a Jack of ";
		break;
	case queen: rank1 = 12;
		cout << "The first card is a Queen of ";
		break;
	case king: rank1 = 13;
		cout << "The first card is a King of ";
		break;
	case ace: rank1 = 14;
		cout << "The first card is an Ace of ";
		break;
	default:
		cout << "Not a card rank";
	}
	switch (card1.suit)
	{
	case heart: cout << "Heart.\n";
		break;
	case diamond: cout << "Diamond.\n";
		break;
	case club: cout << "Club.\n";
		break;
	case spade: cout << "Spade.\n";
		break;
	default:
		cout << "Not a card rank";
	}


	switch (card2.rank)
	{
	case two: rank2 = 2;
		cout << "The second card is a Two of ";
		break;
	case three: rank2 = 3;
		cout << "The second card is a Three of ";
		break;
	case four: rank2 = 4;
		cout << "The second card is a Four of ";
		break;
	case five: rank2 = 5;
		cout << "The second card is a Five of ";
		break;
	case six: rank2 = 6;
		cout << "The second card is a Six of ";
		break;
	case seven: rank2 = 7;
		cout << "The second card is a Seven of ";
		break;
	case eight: rank2 = 8;
		cout << "The second card is a Eight of ";
		break;
	case nine: rank2 = 9;
		cout << "The second card is a Nine of ";
		break;
	case ten: rank2 = 10;
		cout << "The second card is a Ten of ";
		break;
	case jack: rank2 = 11;
		cout << "The second card is a Jack of ";
		break;
	case queen: rank2 = 12;
		cout << "The second card is a Queen of ";
		break;
	case king: rank2 = 13;
		cout << "The second card is a King of ";
		break;
	case ace: rank2 = 14;
		cout << "The second card is an Ace of ";
		break;
	default:
		cout << "Not a card rank";
	}
	switch (card2.suit)
	{
	case heart: cout << "Heart.\n";
		break;
	case diamond: cout << "Diamond.\n";
		break;
	case club: cout << "Club.\n";
		break;
	case spade: cout << "Spade.\n";
		break;
	default:
		cout << "Not a card rank";
	}

	if (rank1 < rank2) 
	{
		cout << "The second card has the higher rank.";
	}
	else if (rank2 < rank1)
	{
		cout << "The first card has the higher rank.";
	}
	else
	{
		cout << "These cards' ranks are equal.";
	}
	return card1;
	return card2;
}

int main()
{
	Card card;
	card.rank = ace;
	card.suit = spade;

	PrintCard(card);

	Card card1;
	card1.rank = jack;
	card1.suit = heart;
	Card card2;
	card2.rank = queen;
	card2.suit = club;

	HighCard(card1, card2);

	_getch();
	return 0;
}